import getopt
import sys
import json
from os import error
from car.databases.client import MongoLsv
from bson.objectid import ObjectId
from car.solutions.create import create_record
from car.solutions.update import update_record
from car.solutions.delete import delete_record

mongo_lsv = MongoLsv()

opts, args = getopt.getopt(sys.argv[1:], 'd:c:r:i:v:')
script = sys.argv[0]

database = None
collection = None
request = None
record_id = None
value = {}

for opt, arg in opts:
    if opt == "-d":
        database = arg
    elif opt == "-c":
        collection = arg
    elif opt == "-r":
        request = arg
    elif opt == "-i":
        record_id = arg
    elif opt == "-v":
        try:
            value = json.loads(arg)
        except Exception as e:
            print(e)
            error("option -v not valid")
            sys.exit(0)

def look_for_objectid(record):
    for key, value in record.items():
        if 'dict' in str(type(value)):
            record[key] = ObjectId(value['ObjectId'])

    return record

value = look_for_objectid(value)

if database is None:
    error("option -db not set")
    sys.exit(0)

if collection is None:
    error("option -c not set")
    sys.exit(0)

if request is None:
    error("option -r not set")
    sys.exit(0)


# Examples of create record:
# python3 manage.py -d car_lsv -c job -r create -d '{"job_type":"chef"}'
# python3 manage.py -d car_lsv -c job_offer -r create -v '{"title":"Looking for teacher", "description": "teacher for kids", "salary": 1500000.0, "company": "utb", "city": "Cartagena", "job": {"ObjectId": "62b0dfa9b9013f82902d7d3c"}}'

if request == 'create':
    create_record(database, collection, value)


# Examples of update record:
# python3 manage.py -d car_lsv -c job -r update -i 62b0b18ed7bf5d1be29fe9e4 -v '{"job_type":"plumber"}'
# python3 manage.py -d car_lsv -c job_offer -r update -i 62b0f8e1475a7797a36241b4 -v '{"salary": 4700000}'

if request == 'update':
    update_record(database, collection, record_id, value)


# Examples of delete a record:
# python3 manage.py -d car_lsv -c job -r delete -i 62b0b18ed7bf5d1be29fe9e4
# python3 manage.py -d car_lsv -c job_offer -r delete -i 62b0f8e1475a7797a36241b4
if request == 'delete':
    delete_record(database, collection, record_id)

print(
    mongo_lsv.get_records_from_collection(
        db_name="car_lsv", collection=collection
    )
)

