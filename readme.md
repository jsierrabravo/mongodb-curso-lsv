# MongoDB LSV

### Desarrollador: Juan Sebastián Sierra Bravo

MongoDb LSV es un repositorio que se usara para el curso del Car

# Final curso de MONGODB

Examen final

 - **Cada estudiante tiene su propio repositorio con su respectivo tema**
 - **Se crearan una interface de las respectivas tablas ejemplo Departamento / Municipio:**
 - **Se crearan en la consola de mongodb la base de datos:**
 - **Haran un archivo por cada operacion del crud ejemplo:**

```sh
    # Register new Record in Collection
    $ python3 register.py
    $ python3 update.py
    $ python3 delete.py
```

- **Haran un archivo por cada operacion del crud ejemplo:**
- **Siempre llamar get_records_from_collection al final de cada archivo:**
- **Anexar Resultados:**
- **Crear un PullRequest en sus propias branch con la respectiva solucion:**



https://gitlab.com/jsierrabravo/mongodb-curso-lsv -> 
```sh
Juan Sierra = trabajo / oferta_de_trabajo
```

### Requeriments

 - Docker
 - docker-compose

 - **Iniciar el Mongo:**
```sh
$ git clone https://gitlab.com/valentinc94/mongodb-curso-lsv.git
$ docker-compose up
```

 - **Contenedor docker:**
```sh
$ docker exec -it docker_id /bin/bash
```

```sh
$ mongo
```

### CRUD operations

To make an operation using Python, all data must be passed to the `manage.py` file through the console using the following flags:

```
-d -> database name
-c -> collection name
-r -> request/operation
-i -> record id (mandatory for update and delete operations)
-v -> value/record (mandatory for create and update operations)
```

For example:
```sh
# create
python3 manage.py -d car_lsv -c job -r create -d '{"job_type":"chef"}'

# update
python3 manage.py -d car_lsv -c job -r update -i 62b0b18ed7bf5d1be29fe9e4 -v '{"job_type":"plumber"}'

# delete
python3 manage.py -d car_lsv -c job -r delete -i 62b0b18ed7bf5d1be29fe9e4
```

If an ObjectId must be passed in a record field, the value of this field must be a dictionary with key "ObjectId" and a string containing the id as value. For example:

```sh
python3 manage.py -d car_lsv -c job_offer -r create -v '{"title":"Looking for teacher", "description": "teacher for kids", "salary": 1500000.0, "company": "utb", "city": "Cartagena", "job": {"ObjectId": "62b0dfa9b9013f82902d7d3c"}}'
```

