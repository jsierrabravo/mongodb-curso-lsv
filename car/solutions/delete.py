from car.databases.client import MongoLsv

mongo_lsv = MongoLsv()

def delete_record(database, collection, record_id):
    print(
        mongo_lsv.delete_record_in_collection(
            db_name=database,
            collection=collection,
            record_id=record_id,
        )
    )