from car.databases.client import MongoLsv

mongo_lsv = MongoLsv()

def create_record(database, collection, record):
    print(
        mongo_lsv.create_new_record_in_collection(
            db_name=database,
            collection=collection,
            record=record,
        )
    )