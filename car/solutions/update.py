from car.databases.client import MongoLsv
from bson.objectid import ObjectId

mongo_lsv = MongoLsv()

def update_record(database, collection, record_id, new_value):
    print(
        mongo_lsv.update_record_in_collection(
            db_name=database,
            collection=collection,
            record_query={"_id": ObjectId(record_id)},
            record_new_value=new_value,
        )
    )

