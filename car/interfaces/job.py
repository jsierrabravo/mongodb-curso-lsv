from dataclasses import asdict, dataclass
from datetime import datetime


@dataclass
class Job:
    uuid: str
    job_type: str

    def to_dict(self) -> dict:
        return asdict(self)


@dataclass
class JobOffer:
    uuid: str  # UUID
    title: str
    description: str
    salary: float
    company: str
    city: str
    job: Job

    def to_dict(self) -> dict:
        return asdict(self)

    @property
    def get_job_type(self):
        return self.job.job_type
